<?php
namespace Vendor\Module\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    protected $pageFactory;

    public function __construct(Context $context, PageFactory $pageFactory)
    {
        $this->pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        // wyświetlenie na stronie dowolnej wartości
        echo "Hello world!";
        // zwracanie pustej strony
        return $this->pageFactory->create();
    }
}

// wyswietlenie wstrzyknietej tablicy
public function execute()
{
    $array = $this->getRequest()->getParam('array');
    var_dump($array);
    return $this->pageFactory->create();
}